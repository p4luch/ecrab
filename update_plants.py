import sys
import django
import os

from update_script import Ensembl

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from default.models import EnsemblVersion


class PlantsEnsembl():
    def __init__(self):
        self.ensembl = 'plants'
        self.host = 'ftp://ftp.ensemblgenomes.org'
        self.dir = 'pub/current/plants/mysql'
        self.current_db_version = EnsemblVersion.objects.get(db_name='plants').db_version
        self.db_obj = EnsemblVersion.objects.get(db_name='plants')
        self.PLANTS = ''

    def manager(self):
        self.PLANTS = Ensembl()
        self.update_params()
        self.PLANTS.manager()

    def update_params(self):
        self.PLANTS.dir = self.dir
        self.PLANTS.host = self.host
        self.PLANTS.current_db_version = self.current_db_version
        self.PLANTS.db_obj = self.db_obj
        self.PLANTS.ensembl = self.ensembl