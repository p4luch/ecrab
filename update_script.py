from ftplib import FTP
import os
import gzip
import sys
import django
import shutil
import subprocess
import traceback
from helpers import Helpers


sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from django import db
from django.core.mail import send_mail
from django.conf import settings
from main.models import GeneMain, OrganismMain, BiotypeMain
from backup.models import GeneBackup, OrganismBackup, BiotypeBackup
from default.models import DataBaseStatus
from default.models import EnsemblVersion
from default.models import FileStatus


class Ensembl:
    """
        Check if there is new version of Ensembl Metazoa database,
    download and prepare data for updating E-CRAB database files.
    """
    def __init__(self):
        self.host = ''
        self.dir = ''
        self.ftp = ''
        self.org_list = []
        self.current_db_version = ''
        self.db_obj = ''
        self.base_folder = ''
        self.genes = {}
        self.exons = {}
        self.exon_transcript = {}
        self.seq_regions = {}
        self.xrefs = {}
        self.checksums = {}
        self.translations = {}
        self.object_xrefs = {}
        self.help = Helpers()
        self.orgs_dict = {}
        self.biot_dict = {}
        self.ensembl = ''

    def manager(self):
        """
            Management function, connect, check and download if necessary.
        """
        if self.check_db_version():
            # Clear main
            self.block_main_db()
            self.clear_main()
            # Update database
            self.connect()
            self.download_list_of_organisms()
            self.ftp_close()
            self.create_dirs()
            self.delete_dicts()
            self.upload_orgs_data()
            self.upload_gene_data()
            self.clear_and_copy()
            # Clear backup and upload data from main
            self.clear_backup()
            self.copy_data()

    def connect(self):
        """
            Connect to Ensembl Metazoa FTP server.
        """
        try:
            host = self.host[6:] if self.ensembl != 'main' else self.host
            self.ftp = FTP(host)
            self.ftp.login()
            self.ftp.cwd(self.dir)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at connect. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def download_list_of_organisms(self):
        """
            Get list of organisms.
        """
        try:
            dir_list = []
            dir_list = self.ftp.nlst()
            self.org_list = [org for org in dir_list if 'core' in org ]
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at download_list_of_organisms. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def check_db_version(self):
        """
            Check current Ensembl database version on ftp server.
        """
        try:
            new_db_version = ''
            host = self.host[6:] if self.ensembl != 'main' else self.host
            ftp = FTP(host)
            ftp.login()
            ftp.cwd(self.dir)
            orgs = ftp.nlst()
            ftp.quit()
            orgs = [org for org in orgs if 'core' in org]
            splited = orgs[0].split('_')
            new_db_version = splited[splited.index('core')+1]
            orgs = []
            status = DataBaseStatus.objects.all()[0].status
            if int(new_db_version) > int(self.current_db_version) and status == 'stable':
                self.current_db_version = new_db_version
                return True
            else:
                return False
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at check_db_version. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def create_dirs(self):
        """
            Create folder structre in which new files will be stored,
        download them and parse.
        """
        try:
            name = ''
            self.base_folder = '{0}_release-{1}'.format(self.ensembl, self.current_db_version)
            os.makedirs(os.path.join('DATABASES', 'DATABASE_FILES', self.base_folder))
            for organism in self.org_list:
                tmp = organism.split('_')
                c_index = tmp.index('core')
                name = '_'.join(tmp[:c_index])
                os.makedirs(
                            os.path.join(
                                         'DATABASES',
                                         'DATABASE_FILES',
                                         self.base_folder,
                                         name))
                self.download_files(
                                    os.path.join('DATABASES',
                                                 'DATABASE_FILES',
                                                 self.base_folder,
                                                 name), organism)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at create_dirs. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def download_files(self, path, organism):
        """
            Download list of files needed to generate new database.
        """
        try:
            file_list = ['gene.txt.gz', 'exon.txt.gz',
                            'exon_transcript.txt.gz', 'seq_region.txt.gz',
                            'xref.txt.gz', 'translation.txt.gz',
                            'object_xref.txt.gz', 'meta.txt.gz']
            self.set_checksums(path, file_list, organism)
            checked = 0
            good = False
            for item in file_list:
                checked = 0
                good = False
                while(not good and checked < 3):
                    os.system('wget -q -P {0} {1}'.format(os.path.join(path), self.host+'/'+self.dir+'/'+organism+'/'+item))
                    output = subprocess.Popen(
                                              ['sum', os.path.join(path, item)],
                                              stdout=subprocess.PIPE
                                              ).communicate()[0]
                    output = output.split()[0]
                    if int(output) != int(self.checksums[item]):
                        good = False
                    elif int(output) == int(self.checksums[item]):
                        good = True
                    checked += 1
                if not good and checked == 3:
                    send_mail('Update Fail',
                              'update.py failed to get files. CHECK IT.\n',
                               settings.EMAIL_HOST_USER,
                               ['luk.pauszek@gmail.com'],
                               fail_silently=False)
                    sys.exit()
            self.prepare_files(path)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at download_files. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_files(self, path):
        """
            Manager function for creation dicts and generating output files.
        """
        try:
            for item in os.listdir(path):
                func = self.switcher(item)
                func(path)
            self.generate_genes(path)
            self.generate_exons(path)
            self.generate_ontology(path)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_files. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def switcher(self, key):
        """
            Return name of function which is parsing specified file.
        """
        try:
            return {
                'gene.txt.gz': self.prepare_genes,
                'exon.txt.gz': self.prepare_exons,
                'exon_transcript.txt.gz': self.prepare_exons_transcripts,
                'seq_region.txt.gz': self.prepare_region,
                'xref.txt.gz': self.prepare_xref,
                'translation.txt.gz': self.prepare_translation,
                'object_xref.txt.gz': self.prepare_object_xref,
                'meta.txt.gz': self.get_assembly
            }[key]
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at switcher. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def get_assembly(self, path):
        try:
            abbrev = os.path.basename(os.path.normpath(path))
            meta = gzip.open(os.path.join(path, 'meta.txt.gz'), 'rb').read().splitlines()
            for i, line in enumerate(meta):
                data = line.decode('latin-1').split('\t')
                if 'species.scientific_name' in data:
                    full_name = data[-1]
                if 'assembly.name' in data:
                    assembly = data[-1]
            f = open(os.path.join('DATABASES', 'new_org_{0}.txt'.format(self.ensembl)), 'a')
            f.write('\t'.join([abbrev, full_name, assembly])+'\n')
            f.close()
            os.remove(os.path.join(path, 'meta.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at get_assembly. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def generate_genes(self, path):
        """
        Generate genes.txt file.
        """
        try:
            seq_region = ''
            xref_key = ''
            xref_value = ''
            counter = 0
            with open(os.path.join(path, 'genes.txt'), 'w') as g_file:
                for g_key in self.genes:
                    seq_region, xref_key = g_key.split('|')[:2]
                    values = [val.replace('\\N', '-') for val in self.genes[g_key]]
                    if xref_key == '-':
                        xref_value = '-'
                    else:
                        xref_value = self.xrefs.get(xref_key, '-')[0]
                    line = '\t'.join([values[5], values[1], values[2],
                                     self.seq_regions[seq_region], xref_value,
                                     values[3], values[0], values[4], '\n'])
                    g_file.write(line)
                    counter += 1
                    if counter == 5000:
                        counter = 0
                        g_file.write('xXxXxXxXxXx\n') # tag for dividing file in parts
                g_file.write('xXxXxXxXxXx\n')
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at generate_genes. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def generate_exons(self, path):
        """
            Generate exons.txt file.
        """
        try:
            transcript_id_key = ''
            with open(os.path.join(path, 'exons.txt'), 'w') as e_file:
                for g_key in self.genes:
                    transcript_id_key = g_key.split('|')[2]
                    gene_id = self.genes[g_key][5]
                    if transcript_id_key in self.exon_transcript:
                        for exon in self.exon_transcript[transcript_id_key]:
                            e_file.write('\t'.join([gene_id,
                                         self.exons[exon][0],
                                         self.exons[exon][1], '\n']))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at generate_exons. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def generate_ontology(self, path):
        """
            Generate ontology.txt file.
        """
        try:
            with open(os.path.join(path, 'ontology.txt'), 'w') as o_file:
                for g_key in self.genes:
                    e_gene_id = self.genes[g_key][5]
                    transcript_id = g_key.split('|')[2]
                    if transcript_id in self.translations:
                        translation_id = self.translations[transcript_id]
                        if translation_id in self.object_xrefs:
                            objects_id = self.object_xrefs[translation_id]
                            for item in objects_id:
                                x_terms = self.xrefs[item]
                                if x_terms[1] == '1000':
                                    o_file.write('\t'.join([e_gene_id,
                                                            x_terms[0], '\n']))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed tat generate_ontology. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_genes(self, path):
        """
            Generate dict based on gene.txt.gz file.
        """
        try:
            cols = []
            self.genes = {}
            with gzip.open(os.path.join(path, 'gene.txt.gz'), 'rb') as gene:
                for line in gene:
                    cols = line.decode('latin-1').split('\t')
                    if cols[7] == '\\N':
                        xref = '-'
                    else:
                        xref = cols[7]
                    key = '|'.join([cols[3], xref, cols[12]])
                    value = [cols[1], cols[4], cols[5], cols[6], cols[10], cols[13]]
                    if key not in self.genes:
                        self.genes[key] = value
            os.remove(os.path.join(path, 'gene.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_genes. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_exons(self, path):
        """
            Generate dict based on exon.txt.gz file.
        """
        try:
            cols = []
            self.exons = {}
            with gzip.open(os.path.join(path, 'exon.txt.gz'), 'rb') as exons:
                for line in exons:
                    cols = line.decode('latin-1').split('\t')
                    key = cols[0]
                    value = [cols[2], cols[3]]
                    if key not in self.exons:
                        self.exons[key] = value
            os.remove(os.path.join(path, 'exon.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_exons. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_exons_transcripts(self, path):
        """
            Generate dict based on exon_transcript.txt.gz file.
        """
        try:
            cols = []
            self.exon_transcript = {}
            with gzip.open(os.path.join(path, 'exon_transcript.txt.gz'), 'rb')\
                    as exon_transcripts:
                for line in exon_transcripts:
                    cols = line.decode('latin-1').split('\t')
                    key = cols[1]
                    value = cols[0]
                    if key not in self.exon_transcript:
                        self.exon_transcript[key] = [value]
                    else:
                        self.exon_transcript[key].append(value)
            os.remove(os.path.join(path, 'exon_transcript.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_exons_transcripts. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_region(self, path):
        """
            Generate dict based on seq_region.txt.gz file.
        """
        try:
            cols = []
            self.seq_regions = {}
            with gzip.open(os.path.join(path, 'seq_region.txt.gz'), 'rb')\
                    as seq_region:
                for line in seq_region:
                    cols = line.decode('latin-1').split('\t')
                    key = cols[0]
                    value = cols[1]
                    if key not in self.seq_regions:
                        self.seq_regions[key] = value
            os.remove(os.path.join(path, 'seq_region.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_region. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_xref(self, path):
        """
            Generate dict based on xref.txt.gz file.
        """
        try:
            cols = []
            self.xrefs = {}
            with gzip.open(os.path.join(path, 'xref.txt.gz'), 'rb')\
                    as xref:
                for line in xref:
                    cols = line.decode('latin-1').split('\t')
                    if len(cols) >=4:
                        key = cols[0]
                        value = [cols[3], cols[1]]
                        if key not in self.xrefs:
                            self.xrefs[key] = value
            os.remove(os.path.join(path, 'xref.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_xref. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_object_xref(self, path):
        """
            Generate dict based on object_xref.txt.gz file.
        """
        try:
            cols = []
            self.object_xrefs = {}
            with gzip.open(os.path.join(path, 'object_xref.txt.gz'), 'rb')\
                    as o_xref:
                for line in o_xref:
                    cols = line.decode('latin-1').split('\t')
                    key = cols[1]
                    value = cols[3]
                    if key not in self.object_xrefs:
                        self.object_xrefs[key] = [value]
                    else:
                        self.object_xrefs[key].append(value)
            os.remove(os.path.join(path, 'object_xref.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_object_xref. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def prepare_translation(self, path):
        """
            Generate dict based on translation.txt.gz file.
        """
        try:
            cols = []
            self.translations = {}
            with gzip.open(os.path.join(path, 'translation.txt.gz'), 'rb')\
                    as translation:
                for line in translation:
                    cols = line.decode('latin-1').split('\t')
                    key = cols[1]
                    value = cols[0]
                    if key not in self.translations:
                        self.translations[key] = value
            os.remove(os.path.join(path, 'translation.txt.gz'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at prepare_translation. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def upload_orgs_data(self):
        """
            Upload  data about organism names, chromosomes and biotypes.
        """
        try:
            biot_list = {}
            for folder in os.listdir(os.path.join('DATABASES', 'DATABASE_FILES', self.base_folder)):
                OrganismMain.objects.create(scientific_name=folder, db=self.db_obj)
                with open(os.path.join('DATABASES', 'DATABASE_FILES', self.base_folder, folder, 'genes.txt'), 'r')\
                        as genes:
                    biot_bulk = []
                    for line in genes:
                        if line != 'xXxXxXxXxXx\n':
                            col = line.split('\t')
                            new_biotype = col[6]
                            if new_biotype not in biot_list.keys():
                                biot_bulk.append(BiotypeMain(biotype_name=new_biotype, db=self.db_obj))
                                biot_list[new_biotype] = ''
                    BiotypeMain.objects.bulk_create(biot_bulk)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at upload_orgs_data. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def generate_orgs_dict(self, source):
        try:
            self.orgs_dict = {}
            if source == 'main':
                for org in OrganismMain.objects.all():
                    if org.scientific_name not in self.orgs_dict.keys():
                        self.orgs_dict[org.scientific_name] = org
            elif source == 'backup':
                for org in OrganismBackup.objects.all():
                    if org.scientific_name not in self.orgs_dict.keys():
                        self.orgs_dict[org.scientific_name] = org
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                      'update.py failed at generate_orgs_dict. CHECK IT.\n{0}'.format(tb),
                      settings.EMAIL_HOST_USER,
                      ['luk.pauszek@gmail.com'],
                      fail_silently=False)
            sys.exit()

    def generate_biot_dict(self, source):
        try:
            self.biot_dict = {}
            if source == 'main':
                for biot in BiotypeMain.objects.all():
                    if biot.biotype_name not in self.biot_dict.keys():
                        self.biot_dict[biot.biotype_name] = biot
            elif source == 'backup':
                for biot in BiotypeBackup.objects.all():
                    if biot.biotype_name not in self.biot_dict.keys():
                        self.biot_dict[biot.biotype_name] = biot
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                      'update.py failed at upload_orgs_data. CHECK IT.\n{0}'.format(tb),
                      settings.EMAIL_HOST_USER,
                      ['luk.pauszek@gmail.com'],
                      fail_silently=False)
            sys.exit()

    def upload_gene_data(self):
        """
            Upload data from each organism to main database.
        """
        try:
            to_bulk = []
            self.generate_orgs_dict('main')
            self.generate_biot_dict('main')
            for folder in os.listdir(os.path.join('DATABASES', 'DATABASE_FILES', self.base_folder)):
                to_bulk = []
                with open(os.path.join('DATABASES', 'DATABASE_FILES', self.base_folder, folder, 'genes.txt'), 'r')\
                        as genes:
                    for line in genes:
                        if line != 'xXxXxXxXxXx\n':
                            col = line.split('\t')
                            to_bulk.append(GeneMain(
                                           db=self.db_obj,
                                           organism=self.orgs_dict[folder],
                                           gene_id=col[0],
                                           start=col[1],
                                           end=col[2],
                                           chr_name=col[3],
                                           asso_gene_name=col[4],
                                           strand=self.help.unify_strand(col[5]),
                                           g_type=self.biot_dict[col[6]],
                                           description=col[7]))
                        if line == 'xXxXxXxXxXx\n':
                            GeneMain.objects.bulk_create(to_bulk)
                            db.reset_queries()
                            to_bulk = []
            self.orgs_dict.clear()
            self.biot_dict.clear()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at upload_gene_data. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def set_checksums(self, path, file_list, organism):
        """
            Build dictionary with file names as keys and checksums as values.
        """
        try:
            self.checksums = {}
            os.system('wget -q -P {0} {1}'.format(os.path.join(path), self.host+'/'+self.dir+'/'+organism+'/'+'CHECKSUMS'))
            with open(os.path.join(path, 'CHECKSUMS'), 'r') as checksums:
                for line in checksums:
                    if self.ensembl == 'main':
                        ch_sums, file_name = line.strip().split('\t')
                        ch_sums = ch_sums.split(' ')[::len(ch_sums.split(' ')) - 1]
                    else:
                        tmp_list = line.strip().split()
                        ch_sums = tmp_list[0]
                        file_name = tmp_list[-1]
                    for item in file_list:
                        if item == file_name:
                            if file_name not in self.checksums:
                                if self.ensembl == 'main':
                                    self.checksums[file_name] = ch_sums[0]
                                else:
                                    self.checksums[file_name] = ch_sums
            os.remove(os.path.join(path, 'CHECKSUMS'))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at set_checksums. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def clear_and_copy(self):
        """
            Manager function to copy new files into right directory and
        remove  leftover folders and files.
        """
        try:
            self.delete_genes_files()
            self.block_database_files()
            self.remove_old_files()
            self.rename_new_folder()
            self.unlock_database_files()
            self.unlock_database_and_folders()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at clear_and_copy. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def block_database_files(self):
        """
            Block query submission to work with DATABASE_FILES folder.
        """
        try:
            f_stat = FileStatus.objects.get(pk=1)
            f_stat.status = 'update'
            f_stat.save()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at block_database_files. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def unlock_database_files(self):
        """
            Unlcok query submisson.
        """
        try:
            f_stat = FileStatus.objects.get(pk=1)
            f_stat.status = 'stable'
            f_stat.save()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at unblock_databases_files. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def delete_genes_files(self):
        """
            Delete files with gene data from new release folder.
        """
        try:
            path = os.path.join('DATABASES', 'DATABASE_FILES', self.base_folder)
            for folder in os.listdir(path):
                for item in os.listdir(os.path.join(path, folder)):
                    if item == 'genes.txt':
                        os.remove(os.path.join(path, folder, item))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at delete_genes_files. CHECK IT.\n{0}'.format(tb),
                          settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def delete_dicts(self):
        """
            Clear dictionares after last organism.
        """
        self.genes.clear()
        self.exons.clear()
        self.exon_transcript.clear()
        self.seq_regions.clear()
        self.xrefs.clear()
        self.checksums.clear()
        self.translations.clear()
        self.object_xrefs.clear()

    def remove_old_files(self):
        """
            Delete folder DATABASE_FILES with old data.
        """
        try:
            shutil.rmtree(os.path.join('DATABASES', 'DATABASE_FILES', self.ensembl))
            os.remove(os.path.join('DATABASES', 'org_{0}.txt'.format(self.ensembl)))
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at remove_old_files. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def rename_new_folder(self):
        """
            Rename new realease folder to 'DATABASE_FILES'
        """
        try:
            old = os.path.join('DATABASES', 'DATABASE_FILES', self.base_folder)
            new = os.path.join('DATABASES', 'DATABASE_FILES', self.ensembl)
            old2 = os.path.join('DATABASES', 'new_org_{0}.txt'.format(self.ensembl))
            new2 = os.path.join('DATABASES', 'org_{0}.txt'.format(self.ensembl))
            os.rename(old, new)
            os.rename(old2, new2)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at rename_new_folder. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def ftp_close(self):
        """
            Close ftp connection.
        """
        try:
            self.ftp.quit()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at ftp_closes. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def unlock_database_and_folders(self):
        """
            Change DB status to 'stable' and change Ensembl version
        """
        try:
            e_ver = EnsemblVersion.objects.get(db_name=self.ensembl)
            e_ver.db_version = self.current_db_version
            e_ver.save()
            dbs = DataBaseStatus.objects.get(pk=1)
            dbs.status = 'stable'
            dbs.save()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at unlock_database_and_folders. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def clear_backup(self):
        """
            Clear backup database from previous Ensembl data version.
        """
        try:
            GeneBackup.objects.filter(db=self.db_obj).delete()
            OrganismBackup.objects.filter(db=self.db_obj).delete()
            BiotypeBackup.objects.filter(db=self.db_obj).delete()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                      'update.py failed at clear_backup. CHECK IT.\n{0}'.format(tb),
                      settings.EMAIL_HOST_USER,
                      ['luk.pauszek@gmail.com'],
                      fail_silently=False)
            sys.exit()

    def clear_main(self):
        """
            Clear main database from previous Ensembl data version.
        """
        try:
            GeneMain.objects.filter(db=self.db_obj).delete()
            OrganismMain.objects.filter(db=self.db_obj).delete()
            BiotypeMain.objects.filter(db=self.db_obj).delete()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                      'update.py failed at clear_main. CHECK IT.\n{0}'.format(tb),
                      settings.EMAIL_HOST_USER,
                      ['luk.pauszek@gmail.com'],
                      fail_silently=False)
            sys.exit()

    def copy_organisms(self):
        """
            Copy organism names from main to backup 
        """
        try:
            to_bulk = []
            for org in OrganismMain.objects.all():
                to_bulk.append(OrganismBackup(scientific_name=org.scientific_name, db=self.db_obj))
            OrganismBackup.objects.bulk_create(to_bulk)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                      'update.py failed at copy_organism. CHECK IT.\n{0}'.format(tb),
                      settings.EMAIL_HOST_USER,
                      ['luk.pauszek@gmail.com'],
                      fail_silently=False)
            sys.exit()


    def copy_biotypes(self):
        """
            Copy biotype names from main to backup
        """
        try:
            to_bulk = []
            for biot in BiotypeMain.objects.all():
                to_bulk.append(BiotypeBackup(biotype_name=biot.biotype_name, db=self.db_obj))
            BiotypeBackup.objects.bulk_create(to_bulk)
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                      'update.py failed at copy_biotypes. CHECK IT.\n{0}'.format(tb),
                      settings.EMAIL_HOST_USER,
                      ['luk.pauszek@gmail.com'],
                      fail_silently=False)
            sys.exit()

    def copy_data(self):
        """
            Copy data from main database to backup.
        """
        try:
            self.copy_organisms()
            self.copy_biotypes()
            self.generate_orgs_dict('backup')
            self.generate_biot_dict('backup')
            for org in self.orgs_dict:
                to_bulk = []
                g_count = GeneMain.objects.filter(organism__scientific_name=org, db=self.db_obj).count()
                start = 0
                end = 5000
                if g_count < 5000:
                    end = g_count
                while True:
                    for gene in GeneMain.objects.filter(organism__scientific_name=org, db=self.db_obj).order_by('pk')[start:end]:
                        to_bulk.append(GeneBackup(
                            organism=self.orgs_dict[org],
                            gene_id=gene.gene_id,
                            start=gene.start,
                            end=gene.end,
                            chr_name=gene.chr_name,
                            asso_gene_name=gene.asso_gene_name,
                            strand=gene.strand,
                            g_type=self.biot_dict[gene.g_type.biotype_name],
                            description=gene.description,
                            db=self.db_obj
                        ))
                    GeneBackup.objects.bulk_create(to_bulk)
                    db.reset_queries()
                    to_bulk = []
                    if end == g_count:
                        break
                    start += 5000
                    end += 5000
                    if end > g_count:
                        end = g_count
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at copy_data. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()

    def block_main_db(self):
        """
            Change main database status to update.
        """
        try:
            db_status = DataBaseStatus.objects.get(pk=1)
            db_status.status = 'update'
            db_status.save()
        except Exception:
            tb = traceback.format_exc()
            send_mail('Update Fail',
                          'update.py failed at block_main_db. CHECK IT.\n{0}'.format(tb),
                           settings.EMAIL_HOST_USER,
                           ['luk.pauszek@gmail.com'],
                           fail_silently=False)
            sys.exit()
