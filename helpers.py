class Helpers:
    def unify_strand(self, strand):
        """
            Change strand symbol in query.data to match database annotation.
        """
        if strand == '-' or strand == '-1':
            return '-'
        elif strand == '+' or strand == '1':
            return '+'

    def int_check(self, number):
        try:
            int(number)
            return True
        except ValueError:
            return False
