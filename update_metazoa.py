import sys
import django
import os

from update_script import Ensembl

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from default.models import EnsemblVersion


class MetazoaEnsembl():
    def __init__(self):
        self.ensembl = 'metazoa'
        self.host = 'ftp://ftp.ensemblgenomes.org'
        self.dir = 'pub/current/metazoa/mysql'
        self.current_db_version = EnsemblVersion.objects.get(db_name='metazoa').db_version
        self.db_obj = EnsemblVersion.objects.get(db_name='metazoa')
        self.METAZOA = ''

    def manager(self):
        self.METAZOA = Ensembl()
        self.update_params()
        self.METAZOA.manager()

    def update_params(self):
        self.METAZOA.dir = self.dir
        self.METAZOA.host = self.host
        self.METAZOA.current_db_version = self.current_db_version
        self.METAZOA.db_obj = self.db_obj
        self.METAZOA.ensembl = self.ensembl