import sys
import django
import os

from update_script import Ensembl

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from default.models import EnsemblVersion


class MainEnsembl():
    def __init__(self):
        self.ensembl = 'main'
        self.host = 'ftp.ensembl.org'
        self.dir = 'pub/current_mysql'
        self.current_db_version = EnsemblVersion.objects.get(db_name='main').db_version
        self.db_obj = EnsemblVersion.objects.get(db_name='main')
        self.MAIN = ''

    def manager(self):
        self.MAIN = Ensembl()
        self.update_params()
        self.MAIN.manager()

    def update_params(self):
        self.MAIN.dir = self.dir
        self.MAIN.host = self.host
        self.MAIN.current_db_version = self.current_db_version
        self.MAIN.db_obj = self.db_obj
        self.MAIN.ensembl = self.ensembl
