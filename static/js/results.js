//Advanced search logic
var search_results;
var fnAddSearchRow = function () {
    var lastdiv = $('#adv_search_rows :last-child');
    var panel = $('#adv_search_rows');
    var new_id;
    for (var key in lastdiv) {
        if (typeof lastdiv[key] === 'object') {
            if (lastdiv[key].tagName == 'DIV') {
                new_id = parseInt(lastdiv[key].id) + 1;
                if (lastdiv[key].id === '1') {
                    $('div#1').append('<a class="glyphicon glyphicon-minus-sign" onclick="fnRemoveSearchRow(this.parentNode.id)"></a>');
                }
            }
        }
    }
    lastdiv.remove('a.glyphicon-plus-sign');
    panel.append("<div id='" + new_id + "'></div>");
    panel = $('#adv_search_rows #' + new_id);
    var colNames = [];
    var cn = document.getElementById('columnsList');
    for (var i = 0; i < cn.length; i++) {
        colNames.push(cn.options[i].text);
    }
    fnAppendColumnList(panel, colNames);
    panel.append('<input type="text" size=50/>');
    panel.append('<a class="glyphicon glyphicon-minus-sign" onclick="fnRemoveSearchRow(this.parentNode.id)"></a><a class="glyphicon glyphicon-plus-sign" onclick="fnAddSearchRow()"></a>');
};

var fnRemoveSearchRow = function (row_id) {
    var row_to_remove = $('#adv_search_rows #' + row_id);
    row_to_remove.remove();
    fnRenumerateSearchRows();
    fnAddPlusGlyphToLast();
    fnRemoveMinusFromFirst();
};

var fnRenumerateSearchRows = function () {
    var panel = document.getElementById('adv_search_rows')
    var children = panel.getElementsByTagName('div');
    for(var i=0; i<children.length; i++){
        children[i].id = i+1;
    }
};

var fnRemoveMinusFromFirst = function () {
    var panel_divs = document.getElementById('adv_search_rows').getElementsByTagName('div');
    for(var i=0; i<panel_divs.length; i++){
        if(panel_divs[i].id === '1') {
            if(panel_divs.length === 1){
                $('div#'+panel_divs[i].id+' a.glyphicon-minus-sign').remove();
            }
        }
    }
};

var fnAddPlusGlyphToLast = function () {
    var panel_divs = document.getElementById('adv_search_rows').getElementsByTagName('div');
    var last_div = panel_divs[panel_divs.length-1];
    if($(last_div).has('a.glyphicon-plus-sign').length === 0){
        $(last_div).append('<a class="glyphicon glyphicon-plus-sign" onclick="fnAddSearchRow()"></a>');
    }
};

var fnAppendColumnList = function (place, colNames) {
    var sel = $('<select id="columnsList">').appendTo(place);
    var counter = 0;
    colNames.forEach(function (name) {
        sel.append($("<option>").attr('value', counter++).text(name));
    });
};

//======================
$(document).ready(function () {
    $('.download').hide();
    $('.pageheader').height($('#logo').height());
    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) c_end = document.cookie.length;
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    }

    var runAjax = function (n) {
        $.ajax({
            type: "GET",
            url: ROOT_URL + 'default/getbyajax/',
            dataType: "json",
            data: {
                token: $('#token').val(),
                counter: $('#counter').val(),
                processed_lines: $('#processed_lines').val(),
                number_of_runs: n
            },
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (json) {
                if (!json.hasOwnProperty('results')) {
                    runAjax(n + 1);
                }
                else {
                    $('.progress').hide();
                    $('.download').show();
                    search_results = json.results;
                    displayTable(json.results, json.unknown, json.col_names);
                }
            }
        });
    };
    var fetchAjax = function () {
        $.ajax({
            type: "GET",
            url: ROOT_URL + 'default/fetchbyajax/',
            dataType: 'json',
            data: {
                token: $('#token').val()
            },
            beforeSend: function () {
                $('.ajaxContent').html('Fetchnig data...');
            },
            complete: function () {
            },
            success: function (json) {
                $('.progress').hide();
                $('.download').show();
                search_results = json.results;
                displayTable(json.results, json.unknown, json.col_names);
            }
        });
    };
    var fnCreateAdvSearchPanel = function (colNames) {
        $('.adv_search').append('<div class="panel-group borders" id="accordion" role="tablist" aria-multiselectable="true"><div class="panel panel-default"><div class="panel-heading" role="tab" id="headingOne"><h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">Advanced search</a></h4></div><div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><div id="adv_search_rows"></div></div></div></div></div>');
        fnPopulateAdvSearch(colNames);
    };
    var fnPopulateAdvSearch = function (colNames) {
        var panel = $('#adv_search_rows');
        panel.after('<button id="adv_search_button" class="btn btn-primary">Advanced filter</button>');
        panel.after('<button id="adv_search_reset" class="btn btn-warning">Reset filter</button>');
        $('#adv_search_reset').on('click', function () {
            var dtable = $('#dtable').DataTable();
            dtable
                .clear()
                .rows.add(search_results)
                .draw();
        });
        panel.append("<div id='1'></div>");
        panel = $('#adv_search_rows #1');
        fnAppendColumnList(panel, colNames);
        panel.append('<input type="text" size="50"/>');
        panel.append('<a id="add_logic" class="glyphicon glyphicon-plus-sign" onclick="fnAddSearchRow()"></a>');
        $('#adv_search_button').on('click', function () {
            var rows = [];
            var dtable = $('#dtable').DataTable();
            dtable
                .clear()
                .rows.add(search_results);
            var search_rows = $('#adv_search_rows').children(); // get all search rows
            for(var i=0; i<search_rows.length; i++){  // iterate over them
                var field = '';
                var term = '';
                row_fields = $(search_rows[i]).children(); // get fields in row
                for(var j=0; j<row_fields.length; j++){ // iterate over them
                    if(row_fields[j].tagName === 'SELECT' && row_fields[j].id === 'columnsList'){
                        field = $(row_fields[j]).find(':selected').val();
                    }
                    if(row_fields[j].tagName === 'INPUT'){
                        term = $(row_fields[j]).val();
                    }
                }
                rows = dtable.column(field).search(term).rows({ filter : 'applied'}).data();
            }
            dtable.clear();
            dtable.search('').columns().search('');
            dtable.rows.add(rows).draw();
        });
    };
    var displayTable = function (rows, unknown, col_names) {
        $('.ajaxContent').html('<table class="display" id="dtable"></table>');
        if (unknown == "unknown") {
            $('.unknown_genes').html('<span style="color:red;">Warning!</span> Data contains invalid chromosome names. ');
            $('.unknown_genes').css("display", "inline");
            var h1_text = $('h1').text();
            var sep_index = h1_text.indexOf(':')
            var slug = h1_text.substring(sep_index+1).replace(' ', '');
            $('.unknown_genes').append('<a href="/ecrab/default/chrom_down/'+slug+'">List of valid Ensembl chromosome IDs.</a>');
        }
        $('#dtable').DataTable({
            dom: 'Blf<"adv_search">tip',
            autoWidth: false,
            data: rows,
            lengthMenu: [15, 25, 50, 100],
            pageLength: 25,
            order: [],
            colReorder: true,
            scrollX: true,
            buttons: [
                'colvis',
                {
                    extend: 'colvisGroup',
                    text: 'Show all',
                    show: ':hidden'
                }
            ],
            drawCallback: function () {
                $('html, body').animate({scrollTop: 0}, 100);
                $(".paginate_button").unbind('click', paginateScroll);
                $(".paginate_button").bind('click', paginateScroll);
            },
            columns: fnSetDataTableColumnTitles(col_names),
            initComplete: fnCreateAdvSearchPanel(col_names)
        });
    };

    var fnSetDataTableColumnTitles = function (names) {
        var result = [];
        names.forEach(function (name) {
            result.push({'title': name});
        });
        return result;
    };

    var fnGetVisibleColumns = function (oTable) {
        var counter = 0;
        aColumns = new Array();
        oTable.columns().every(function () {
            if (this.visible()) {
                aColumns.push(this.index())
            }
        });
        return aColumns
    };

    $('a#filter_download').click(function (event) {
        event.preventDefault();
        var aVisibleColumns = fnGetVisibleColumns($('#dtable').DataTable());
        $('#filtered_data').val('');
        var results = $('#dtable').DataTable().rows({
            order: 'applied',
            page: 'all',
            search: 'applied'
        }).data();
        results.each(function (value, index) {
            var filter = [];
            for (var i = 0; i < aVisibleColumns.length; i++) {
                filter.push(value[aVisibleColumns[i]]);
            }
            if (!$('#filtered_data').val()) {
                $('#filtered_data').val(filter.join('\t') + '\n');
            }
            else {
                $('#filtered_data').val($('#filtered_data').val() + filter.join('\t') + '\n');
            }
        });
        $('#fd_send').submit();
    });
    var sec = 0;

    function pad(val) {
        return val > 9 ? val : "0" + val;
    }

    setInterval(function () {
        if ($('#status').val() != 'finished') {
            $("#seconds").html(pad(++sec % 60));
            $("#minutes").html(pad(parseInt(sec / 60, 10)));
        } else {
            $('.clock').hide();
        }
    }, 1000);
    if ($('#status').val() == 'pending') {
        runAjax(0);
    }
    else if ($('#status').val() == 'finished') {
        fetchAjax();
    }
});
