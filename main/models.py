from django.db import models
from default.models import EnsemblVersion

# Create your models here.


class BiotypeMain(models.Model):
    biotype_name = models.CharField(max_length=50)
    db = models.ForeignKey(EnsemblVersion)


class OrganismMain(models.Model):
    scientific_name = models.CharField(max_length=50)
    db = models.ForeignKey(EnsemblVersion)


class GeneMain(models.Model):
    organism = models.ForeignKey(OrganismMain)
    gene_id = models.CharField(max_length=50)
    start = models.IntegerField()
    end = models.IntegerField()
    chr_name = models.CharField(max_length=100)
    asso_gene_name = models.CharField(max_length=50)
    strand = models.CharField(max_length=1)
    g_type = models.ForeignKey(BiotypeMain)
    description = models.TextField()
    db = models.ForeignKey(EnsemblVersion)

    def __str__(self):
        return self.organism.scientific_name+' '+self.chr_name+' '+self.gene_id

    def get_description(self):
        return 'SEP\tSEP'.join([
                         self.chr_name,
                         self.gene_id,
                         self.asso_gene_name,
                         str(self.start),
                         str(self.end),
                         self.strand,
                         self.g_type.biotype_name,
                         self.description
                         ])

