class SelectMainDatabase(object):
    """
    A router to control all database operations on models in the
    main application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read main models go to main_db.
        """
        if model._meta.app_label == 'main':
            return 'main_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write main models go to mian_db.
        """
        if model._meta.app_label == 'main':
            return 'main_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the main app is involved.
        """
        if obj1._meta.app_label == 'main' or \
                obj2._meta.app_label == 'main':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the main app only appears in the 'main_db'
        database.
        """
        if app_label == 'main':
            return db == 'main_db'
        return None

