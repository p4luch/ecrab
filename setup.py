import sys
import os
import django
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from ecrab.settings import BASE_DIR
from default.models import *

pyt_path = os.path.split(BASE_DIR)[0]
os.system("{0}/bin/python3 manage.py migrate default --database='default'".format(pyt_path))
os.system("{0}/bin/python3 manage.py migrate main --database='main_db'".format(pyt_path))
os.system("{0}/bin/python3 manage.py migrate backup --database='backup_db'".format(pyt_path))

FileStatus.objects.create(status='stable')
DataBaseStatus.objects.create(status='stable')
EnsemblVersion.objects.create(db_name='main', db_version='30')
EnsemblVersion.objects.create(db_name='metazoa', db_version='30')
EnsemblVersion.objects.create(db_name='fungi', db_version='30')
EnsemblVersion.objects.create(db_name='plants', db_version='30')
EnsemblVersion.objects.create(db_name='protists', db_version='30')

os.system("{0}/bin/python3 manage.py collectstatic".format(pyt_path))

os.system("mkdir {0}/media".format(BASE_DIR))

os.system("mkdir {0}/DATABASES/DATABASE_FILES".format(BASE_DIR))

os.system("mkdir {0}/DATABASES/DATABASE_FILES/fungi".format(BASE_DIR))
os.system("mkdir {0}/DATABASES/DATABASE_FILES/main".format(BASE_DIR))
os.system("mkdir {0}/DATABASES/DATABASE_FILES/metazoa".format(BASE_DIR))
os.system("mkdir {0}/DATABASES/DATABASE_FILES/plants".format(BASE_DIR))
os.system("mkdir {0}/DATABASES/DATABASE_FILES/protists".format(BASE_DIR))
