import sys
import django
import os

from update_script import Ensembl

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from default.models import EnsemblVersion


class FungiEnsembl():
    def __init__(self):
        self.ensembl = 'fungi'
        self.host = 'ftp://ftp.ensemblgenomes.org'
        self.dir = 'pub/current/fungi/mysql'
        self.current_db_version = EnsemblVersion.objects.get(db_name='fungi').db_version
        self.db_obj = EnsemblVersion.objects.get(db_name='fungi')
        self.FUNGI = ''

    def manager(self):
        self.FUNGI = Ensembl()
        self.update_params()
        self.FUNGI.manager()

    def update_params(self):
        self.FUNGI.dir = self.dir
        self.FUNGI.host = self.host
        self.FUNGI.current_db_version = self.current_db_version
        self.FUNGI.db_obj = self.db_obj
        self.FUNGI.ensembl = self.ensembl
