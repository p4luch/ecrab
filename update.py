from update_protists import ProtistsEnsembl
from update_plants import PlantsEnsembl
from update_metazoa import MetazoaEnsembl
from update_fungi import FungiEnsembl
from update_main import MainEnsembl

if __name__ == '__main__':
    MAIN = MainEnsembl()
    MAIN.manager()
    METAZOA = MetazoaEnsembl()
    METAZOA.manager()
    PLANTS = PlantsEnsembl()
    PLANTS.manager()
    FUNGI = FungiEnsembl()
    FUNGI.manager()
    PROTISTS = ProtistsEnsembl()
    PROTISTS.manager()
