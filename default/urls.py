from django.conf.urls import url

from default import views

urlpatterns = [
    url(r'^help/$', views.help, name='help'),
    url(r'^results/(?P<slug>\d+)/$',
        views.results, name='results'),
    url(r'^retrieve/$', views.retrieve, name='retrieve'),
    url(r'^getbyajax/$', views.getbyajax, name='getbyajax'),
    url(r'^fetchbyajax/$',
        views.fetchbyajax, name='fetchbyajax'),
    url(r'^download/$', views.download, name='download'),
    url(r'^download_merged/$', views.download_merged,
        name='download_merged'),
    url(r'^download_filtered/$', views.download_filtered,
        name='download_filtered'),
    url(r'^get_orgs/$', views.get_orgs, name='get_orgs'),
    url(r'^chrom_down/(?P<slug>\d+)$', views.chrom_download, name='chrom_down')
]