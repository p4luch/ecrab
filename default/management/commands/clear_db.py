from django.core.management.base import BaseCommand
from django.utils import timezone
from default.models import Query, Results
from django.db import connection


class Command(BaseCommand):
    help = 'Clears records in DataBase older than 7 days.'

    def handle(self, **options):
        list_to_delete = []
        now = timezone.now()
        results = Results.objects.all()
        for result in results:
            delta = now - result.pub_date
            if delta.days >= 7:
                list_to_delete.append(result.job_id)
        for item in list_to_delete:
            del_r = Results.objects.get(job_id=item)
            del_r.delete()
            del_q = Query.objects.get(slug=item)
            del_q.delete()
        cursor = connection.cursor()
        cursor.execute("vacuum")
