from django.db import models
from django.template.defaultfilters import slugify
from django.utils.timezone import now

# Create your models here.


class Query(models.Model):
    organism = models.CharField(max_length=100)
    strand = models.BooleanField(default=False)
    ontology = models.BooleanField(default=False)
    exons = models.BooleanField(default=False)
    data = models.TextField()
    email = models.EmailField()
    data_format = models.CharField(max_length=50)
    columns_list = models.CharField(max_length=50)
    slug = models.SlugField(unique=True, default='')
    separator = models.CharField(max_length=50, default='\t')
    db = models.CharField(max_length=50)

    def save(self, *args, **kwargs):
        timestamp = now()
        self.slug = slugify(str(timestamp.second)+str(timestamp.microsecond))
        super(Query, self).save(*args, **kwargs)


class Results(models.Model):
    job_id = models.CharField(max_length=50)
    query_results = models.TextField()
    pub_date = models.DateTimeField()


class EnsemblVersion(models.Model):
    db_name = models.CharField(default='main', max_length=50)
    db_version = models.CharField(max_length=50)


class DataBaseStatus(models.Model):
    status = models.CharField(max_length=50)

    def get_status(self):
        return self.status


class FileStatus(models.Model):
    status = models.CharField(max_length=50)