import time
import os
import sys
import subprocess

from django.shortcuts import render, redirect
from django.http import JsonResponse, StreamingHttpResponse
from django.core.urlresolvers import reverse

# Create your views here.
from .forms import QueryForm, RetrieveForm
from .models import Results, Query, DataBaseStatus
from ecrab.settings import BASE_DIR
from main.models import GeneMain
from backup.models import GeneBackup


def home(request):
    form = QueryForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        # Get values
        query_data = {}
        query_data['organism'] = request.POST['organism']
        query_data['strand'] = serializeCheckbox(request.POST.get('strand', 'off'))
        query_data['ontology'] = serializeCheckbox(request.POST.get('ontology', 'off'))
        query_data['exons'] = serializeCheckbox(request.POST.get('exons', 'off'))
        query_data['email'] = request.POST.get('email', '')
        query_data['separator'] = request.POST['separator']
        query_data['db'] = request.POST['database']
        if not request.FILES:
            query_data['data'] = request.POST['data']
        elif request.FILES:
            fname = request.FILES['file'].name + request.POST['timestamp']
            parent_dir = os.path.abspath(
                                        os.path.join(os.path.dirname(__file__),
                                                     os.pardir))
            fpath = os.path.join(parent_dir, 'media', fname)
            with open(fpath, 'r') as data:
                query_data['data'] = data.read()
            os.remove(os.path.join(fpath))
        if serializeCheckbox(request.POST.get('bed', 'off')):
            query_data['data_format'] = "BED"
        elif serializeCheckbox(request.POST.get('custom', 'off')):
            query_data['data_format'] = "custom"
            col_order = []
            col_order.append(str(int(request.POST['c_chromosome'])-1))
            col_order.append(str(int(request.POST['c_start'])-1))
            col_order.append(str(int(request.POST['c_end'])-1))
            if query_data['strand']:
                col_order.append(str(int(request.POST['c_strand'])-1))
            query_data['columns_list'] = ",".join(col_order)
        q = Query(**query_data)
        q.save()

        return redirect('results', slug=q.slug)
    else:
        parent = os.path.abspath(
            os.path.join(os.path.dirname(__file__), os.pardir))
        organisms = load_orgs_main(parent)
        return render(request, 'home.html', {'form': form, 'errors': form.errors, 'timestamp': time.time(), 'organisms': organisms})


def serializeCheckbox(value):
    if value == 'on':
        return True
    elif value == 'off':
        return False


def help(request):
    return render(request, 'help.html')


def check_status(token):
    try:
        r = Results.objects.get(job_id=token)
        return "finished"
    except Results.DoesNotExist:
        return "pending"


def retrieve(request):
    form = RetrieveForm(request.POST or None)
    if form.is_valid():
        return redirect('results', slug=request.POST['job_id'])
    else:
        return render(request, 'retrieve.html', {'form': form, 'errors': form.errors})


def results(request, slug):
    job_status = check_status(slug)
    q = Query.objects.get(slug=slug)
    if job_status == 'pending':
        line_total = q.data.count('\n')
        if line_total == 0:
            line_total = 1
    elif job_status == 'finished':
        line_total = 0
    return render(request, 'results.html', {'counter': line_total, 'token': q.slug, 'status': job_status})


def getbyajax(request):
    resp_data = {}
    resp_data['unknown'] = ''
    time.sleep(0.5)
    q = Query.objects.get(slug=request.GET['token'])
    token = str(q.pk)
    counter = int(request.GET['counter'])
    number_of_runs = int(request.GET['number_of_runs'])
    if number_of_runs == 0:
        domain = request.build_absolute_uri(reverse('home'))
        os.system("{3}/bin/python3 {2}/default/algorithm.py {0} {1}&".format(token, domain, BASE_DIR,
                                                                             os.path.split(BASE_DIR)[0]))
        resp_data['processed_lines'] = 0
    else:
        if "%stemp" % q.slug in os.listdir(os.path.join(BASE_DIR, 'assets', 'RESULTS')):
            p = subprocess.Popen(["wc", "-l", os.path.join(BASE_DIR, 'assets', 'RESULTS',
                                 '%stemp') % q.slug], stdout=subprocess.PIPE)
            out, err = p.communicate()
            resp_data['processed_lines'] = int(out.split()[0])
        else:
            resp_data['processed_lines'] = 0
        if resp_data['processed_lines'] >= counter:
            p = subprocess.Popen(["wc", "-l", os.path.join(BASE_DIR, 'assets', 'RESULTS',
                                 '%stemp') % q.slug], stdout=subprocess.PIPE)
            out, err = p.communicate()
            resp_data['processed_lines'] = int(out.split()[0])
            grep = subprocess.Popen(["grep", "-o", "saved", os.path.join(BASE_DIR, 'assets', 'RESULTS', '%stemp') %
                                     q.slug], stdout=subprocess.PIPE)
            wc = subprocess.check_output(("wc", "-l"), stdin=grep.stdout)
            grep.wait()
            if wc.strip() == b"1":
                resp_data['results'] = []
                results = Results.objects.get(job_id=request.GET['token'])
                for line in results.query_results.split('\n'):
                    if line:
                        temp = line.split('SEP\tSEP|SEP\tSEP')
                        user_input = temp[0]
                        annotation = temp[1].split('SEP\tSEP')
                        annotation = annotation[:-1]
                        if 'col_names' not in resp_data.keys():
                            resp_data['col_names'] = get_columns_names(user_input, q.data_format, q.columns_list)
                        if "Invalid" in annotation[0]:
                            resp_data['unknown'] = 'unknown'
                        joined = get_annotation_line(annotation, q.data_format, q.columns_list, user_input)
                        resp_data['results'].append(joined)
                os.remove(os.path.join(BASE_DIR, 'assets', 'RESULTS',
                                       '%stemp') % q.slug)
    return JsonResponse(resp_data)


def fetchbyajax(request):
    resp_data = {'results': []}
    resp_data['unknown'] = ''
    r = Results.objects.get(job_id=request.GET['token'])
    q = Query.objects.get(slug=request.GET['token'])
    for line in r.query_results.split('\n'):
        if line:
            temp = line.split('SEP\tSEP|SEP\tSEP')
            user_input = temp[0]
            annotation = temp[1].split('SEP\tSEP')
            annotation = annotation[:-1]
            if 'col_names' not in resp_data.keys():
                resp_data['col_names'] = get_columns_names(user_input, q.data_format, q.columns_list)
            if "Invalid" in annotation[0]:
                    resp_data['unknown'] = 'unknown'
            joined = get_annotation_line(annotation, q.data_format, q.columns_list, user_input)
            resp_data['results'].append(joined)
    return JsonResponse(resp_data)


def download(request):
    token = request.GET.get('token', '')
    r = Results.objects.get(job_id=token)
    to_send = []
    for line in r.query_results.split('\n'):
        if line:
            tmp1, tmp2 = line.split('SEP\tSEP|SEP\tSEP')
            to_send.append(tmp1+'\t'+tmp2)
    response = StreamingHttpResponse((('\t'.join(line.split('SEP\tSEP'))+'\r\n').encode("ascii", "ignore") for line in to_send))
    response['Content-Disposition'] = "attachment; filename={0}results.txt".format(r.job_id)
    response['Content-Length'] = sys.getsizeof(r.query_results)
    return response


def download_filtered(request):
    data = request.POST.get('filtered_data', '')
    job_id = request.POST.get('job_id', '')
    response = StreamingHttpResponse((line+'\r\n' for line in data.split('\n')))
    response['Content-Disposition'] = "attachment; filename={0}results.txt".format(job_id)
    response['Content-Length'] = sys.getsizeof(data)
    return response


def download_merged(request):
    token = request.GET.get('token', '')
    r = Results.objects.get(job_id=token)
    to_merge = {}
    for line in r.query_results.split('\n'):
        if line:
            tmp1, tmp2 = line.split('SEP\tSEP|SEP\tSEP')
            tmp1 = '\t'.join(tmp1.split('SEP\tSEP'))
            if '\t'.join(tmp1.split('SEP\tSEP')) not in to_merge:
                to_merge['\t'.join(tmp1.split('SEP\tSEP'))] = []
            to_merge[tmp1].append('\t'.join(tmp2.split('SEP\tSEP')))
    merged = []
    for key, values in to_merge.items():
        if len(values) == 1:
            merged.append(key+'\t'+values[0])
        else:
            a = []
            for i, value in enumerate(values):
                if a == []:
                    a = value.split('\t')[:-1]
                else:
                    tmp = value.split('\t')[:-1]
                    for e, t in enumerate(tmp):
                        a[e] = '|'.join([a[e], t])
            m_line = "\t".join([key, '\t'.join(a)])
            merged.append(m_line)
    response = StreamingHttpResponse(((line+'\r\n').encode("ascii", "ignore") for line in merged))
    response['Content-Disposition'] = "attachment; filename={0}results.txt".format(r.job_id)
    response['Content-Length'] = sys.getsizeof(r.query_results)
    return response


def get_orgs(request):
    resp_data = {}
    org_list = []
    file_names = {
        "fungi": "org_fungi.txt",
        "metazoa": "org_metazoa.txt",
        "protists": "org_protists.txt",
        "plants": "org_plants.txt"
    }
    parent = os.path.abspath(
            os.path.join(os.path.dirname(__file__), os.pardir))
    db = request.GET.get("db", "")
    if db == "main":
        org_list = load_orgs_main(parent)
    else:
        f = open(os.path.join(parent, 'DATABASES', file_names[db]), 'r').read().splitlines()
        orgs = [line.split('\t') for line in f]
        orgs = sorted(orgs, key=lambda x: x[0])
        for i, items in enumerate(orgs):
            if items:
                tmp_d = {}
                name_assembly = items[1]+' ('+items[2]+')'
                if items[0] not in tmp_d.keys():
                    tmp_d[items[0]]= ''
                tmp_d[items[0]] = name_assembly
                org_list.append(tmp_d)
        org_list.insert(0, {'default': 'Choose organism...'})
    resp_data['org_list'] = org_list
    return JsonResponse(resp_data)


def load_orgs_main(parent):
    f = open(os.path.join(parent, 'DATABASES', 'org_main.txt'), 'r').read().splitlines()
    orgs = [line.split('\t') for line in f]
    orgs = sorted(orgs, key=lambda x: x[0])
    to_ret = []
    r_popular = []
    popular = ['homo_sapiens', 'gallus_gallus', 'mus_musculus', 'rattus_norvegicus', 'danio_rerio']
    for i, items in enumerate(orgs):
        if items:
            tmp_d = {}
            name_assembly = items[1]+' ('+items[2]+')'
            if items[0] not in tmp_d.keys():
                tmp_d[items[0]] = ''
            tmp_d[items[0]] = name_assembly
            if items[0] in popular:
                r_popular.append(tmp_d)
            else:
                to_ret.append(tmp_d)
    r_popular.insert(0, {'default': 'Choose organism...'})
    to_ret.insert(0, {'default1': '----------------------------------'})
    return r_popular+to_ret


def get_columns_names(user_input, data_format, col_order):
    column_number = len(user_input.split('SEP\tSEP'))
    names = []
    if data_format == 'BED':
        names = ['Input Chrom', 'Input ChromStart', 'Input ChromEnd', 'Input Name', 'Input Score', 'Input Strand',
                 'Input ThickStart', 'Input ThickEnd', 'Input ItemRgb', 'Input BlockCount', 'Input BlockSizes',
                 'Input BlockStarts']
        return names[:column_number] + ['Chr Name', 'Gene ID', 'Gene Name', 'Start', 'End', 'Strand',
                                        'Gene type', 'Description', 'Position', 'Gene Ontology']
    elif data_format == 'custom':
        annotated = []
        nonannotated = []
        for i in range(0, column_number):
            names.append('Input column {0}'.format(str(i+1)))
        col_index = [int(col) for col in col_order.split(',')]
        for key, val in enumerate(names):
            if key in col_index:
                annotated.append(val)
            else:
                nonannotated.append(val)
        return annotated + ['Chr Name', 'Gene ID', 'Gene Name', 'Start', 'End', 'Strand',
                            'Gene type', 'Description', 'Position', 'Gene Ontology'] + nonannotated


def get_annotation_line(annotation, data_format, col_order, user_line):
    result = []
    if data_format == 'BED':
        result = [
                annotation[0],
                annotation[1],
                annotation[2],
                annotation[3],
                annotation[4],
                annotation[5],
                annotation[6].replace("_", " "),
                annotation[7],
                annotation[8],
                annotation[9]
            ]
        result = user_line.split('SEP\tSEP') + result
    elif data_format == 'custom':
        cols_indexes = [int(i) for i in col_order.split(',')]
        user_splited = user_line.split('SEP\tSEP')
        if len(cols_indexes) == 4:
            result = [
                user_splited[cols_indexes[0]],
                user_splited[cols_indexes[1]],
                user_splited[cols_indexes[2]],
                user_splited[cols_indexes[3]],
                annotation[0],
                annotation[1],
                annotation[2],
                annotation[3],
                annotation[4],
                annotation[5],
                annotation[6].replace("_", " "),
                annotation[7],
                annotation[8],
                annotation[9],
            ]
            for key, val in enumerate(user_splited):
                if key not in cols_indexes:
                    result.append(val)
        elif len(cols_indexes) == 3:
            result = [
                user_splited[cols_indexes[0]],
                user_splited[cols_indexes[1]],
                user_splited[cols_indexes[2]],
                annotation[0],
                annotation[1],
                annotation[2],
                annotation[3],
                annotation[4],
                annotation[5],
                annotation[6].replace("_", " "),
                annotation[7],
                annotation[8],
                annotation[9],
            ]
            for key, val in enumerate(user_splited):
                if key not in cols_indexes:
                    result.append(val)
    return result


def chrom_download(request, slug):
    q = Query.objects.get(slug=slug)
    dbs = DataBaseStatus.objects.first()
    if dbs.get_status() == 'stable':
        chromosomes = GeneMain.objects.filter(organism__scientific_name=q.organism).values_list('chr_name').distinct()
    else:
        chromosomes = GeneBackup.objects.filter(organism__scientific_name=q.organism).values_list('chr_name').distinct()
    chromosomes = [chrom[0] for chrom in chromosomes]
    response = StreamingHttpResponse(chrom+'\n' for chrom in chromosomes)
    response['Content-Disposition'] = "attachment; filename=chromosome_list.txt"
    response['Content-Length'] = sys.getsizeof(chromosomes)*2
    return response
