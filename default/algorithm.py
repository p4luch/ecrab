"""
    Annotations module.
    Generate annotation for provided query.
"""

import sys
import os
import django
import re

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from django.conf import settings
from django.utils import timezone
from django.core.mail import send_mail
from default.models import Query, Results, DataBaseStatus, EnsemblVersion
from main.models import GeneMain, OrganismMain
from backup.models import GeneBackup, OrganismBackup


class Annotations(object):
    """
        Groups and manages functions for genome region annotations.
    """
    # pylint: disable=too-many-instance-attributes
    def __init__(self):
        self.query = None
        self.region = {}
        self.exons_dict = {}
        self.ontology_dict = {}
        self.ontology = ''
        self.position = ''
        self.parent_dir = os.path.abspath(
            os.path.join(os.path.dirname(__file__), os.pardir))
        self.counter = 0
        self.genes_dict = {}
        self.domain = sys.argv[2]
        self.seps = {
            'tab': '\t',
            'space': ' ',
            'comma': ',',
            'scol': ';',
            'whitespace': 'white'
        }

    def fetch_query(self, token):
        """
            Get query from database.
            Parameters:
                token : unique idenentifier of submited query
        """
        self.query = Query.objects.get(pk=token)

    def generate_annotation(self, token):
        """
            Menager function.
        """
        self.fetch_query(token)
        r = Results(job_id=self.query.slug,
                    query_results='',
                    pub_date=timezone.now())
        tmp_name = str(self.query.slug) + 'temp'
        self.build_genes_dict()
        tmp_handle = open(os.path.join(self.parent_dir, 'assets', 'RESULTS',
                          tmp_name), 'w')
        for line in self.query.data.split('\n'):
            if line.startswith(('track', '#', 'browser')):
                continue
            if line.strip('\r\n') == '':
                self.counter += 1
                tmp_handle.write(str(self.counter)+'\n')
                continue
            self.counter += 1
            self.parse_data_line(line)
            genes = self.fetch_genes()
            if genes:
                if genes[0] == 'unknown':
                    to_join = self.split_line(line.strip()) + ['|', 'Invalid Chromosome name', '-', '-', '-', '-', '-',
                                                               '-', '-', '-', '-', '\n']
                    r.query_results += 'SEP\tSEP'.join(to_join)
                    tmp_handle.write(str(self.counter)+'\n')
                    continue
                for gene in genes:
                    if self.query.exons:
                        self.position = ''
                        if not self.exons_dict:
                            self.build_exons_dict()
                        self.check_position(gene.gene_id)
                    elif not self.query.exons:
                        self.position = '-'
                    if self.query.ontology:
                        self.ontology = ''
                        if not self.ontology_dict:
                            self.build_ontology_dict()
                        self.fetch_ontology(gene.gene_id)
                    elif not self.query.ontology:
                        self.ontology = '-'
                    to_join = self.split_line(line.strip()) + ['|', gene.get_description(), self.position,
                                                               self.ontology, '\n']
                    r.query_results += 'SEP\tSEP'.join(to_join)
            else:
                to_join = self.split_line(line.strip()) + ['|', '-', '-', '-', '-', '-', '-', 'intergenic', '-', '-',
                                                           '-', '\n']
                r.query_results += 'SEP\tSEP'.join(to_join)
            tmp_handle.write(str(self.counter)+'\n')
        r.save()
        tmp_handle.write('saved')
        tmp_handle.close()
        if self.query.email != '':
            subject = "E-CRAB results"
            url = "{0}default/results/{1}".format(self.domain, self.query.slug)
            msg = """Hello,
your job at E-CRAB Web has just been finished.
You can access results under following address:
{0}
Please remember that results will be stored at our servers for next 7 days.
Best Regards,
E-CRAB""".format(url)
            recipient = self.query.email
            sender = settings.EMAIL_HOST_USER
            subject = "E-CRAB results"
            send_mail(subject, msg, sender, [recipient], fail_silently=False)

    def split_line(self, line):
        if self.seps[self.query.separator] == 'white':
            splited = re.split('\s+', line.strip('\r\n'))
        elif self.seps[self.query.separator] == ' ':
            splited = re.split(' +', line.strip('\r\n'))
        else:
            splited = line.split(self.seps[self.query.separator])
        return splited

    def parse_data_line(self, line):
        """
            Split line to annotate and assign required values
            to class field region.
            Parameters:
                line : one line from submited query
            Eg.
                Y          123   456
                Chromosome Start Stop
        """
        self.region = {}
        splited = self.split_line(line)
        if self.query.data_format == 'BED':
            if self.query.strand:
                self.region['chromosome'] = splited[0]
                self.region['start'] = splited[1]
                self.region['end'] = splited[2]
                self.region['strand'] = unify_strand(splited[5].strip('\r'))
            else:
                self.region['chromosome'] = splited[0]
                self.region['start'] = splited[1]
                self.region['end'] = splited[2]
        elif self.query.data_format == 'custom':
            col_num = self.query.columns_list.split(',')
            if self.query.strand:
                self.region['chromosome'] = splited[int(col_num[0])]
                self.region['start'] = splited[int(col_num[1])]
                self.region['end'] = splited[int(col_num[2])]
                self.region['strand'] = unify_strand(splited[int(col_num[3])].strip('\r'))
            else:
                self.region['chromosome'] = splited[int(col_num[0])]
                self.region['start'] = splited[int(col_num[1])]
                self.region['end'] = splited[int(col_num[2])]

    def fetch_genes(self):
        """
            Fetch all genes overlaped by region.
            Returns gene or list of genes which match search criteria.
        """
        genes = []
        if self.region['chromosome'] not in self.genes_dict:
            genes.append('unknown')
            return genes
        for item in self.genes_dict[self.region['chromosome']]:
            positions = [[int(self.region['start']), int(self.region['end'])],
                                [item.start, item.end]]
            positions.sort()
            if positions[1][0] <= positions[0][1]:
                if self.query.strand:
                    if item.strand == self.region['strand']:
                        genes.append(item)
                else:
                    genes.append(item)
        return genes

    def build_exons_dict(self):
        """
            Build dictionary containing genes Ensembl Gene ID as keys
            and lists of start/end coordinates of gene's exons.
        """
        self.exons_dict = {}
        path = os.path.join(self.parent_dir, 'DATABASES', 'DATABASE_FILES',
                            self.query.db, self.query.organism)
        with open(os.path.join(path, 'exons.txt'), 'r') as temp:
            lines = temp.readlines()
            for line in lines[1:]:
                items = line.strip().split('\t')
                if items[0] not in self.exons_dict:
                    self.exons_dict[items[0]] = []
                self.exons_dict[items[0]].append('\t'.join(items[1:]))

    def build_ontology_dict(self):
        """
            Build dictionary containing genes Ensembl Gene ID as keys
             and lists of GO Accession terms for each gene.
        """
        self.ontology_dict = {}
        path = os.path.join(self.parent_dir, 'DATABASES', 'DATABASE_FILES',
                            self.query.db, self.query.organism)
        with open(os.path.join(path, 'ontology.txt'), 'r') as temp:
            lines = temp.readlines()
            for line in lines[1:]:
                items = line.strip().split('\t')
                if len(items) == 1:
                    continue
                if items[0] not in self.ontology_dict:
                    self.ontology_dict[items[0]] = []
                self.ontology_dict[items[0]].append(items[1])

    def fetch_ontology(self, gene_id):
        """
            Return list of GO Accession terms for gene
            or '-' when there is not one.
        """
        if gene_id in self.ontology_dict:
            self.ontology = ', '.join(self.ontology_dict[gene_id])
        else:
            self.ontology = '-'

    def check_position(self, gene_id):
        """
            Return 'exon' if region lies on specified gene exon,
            'intron' otherwise.
        """
        if gene_id in self.exons_dict:
            for coord in self.exons_dict[gene_id]:
                start, end = coord.split('\t')
                positions = [[
                    int(self.region['start']), int(self.region['end'])],
                    [int(start), int(end)]]
                positions.sort()
                if positions[1][0] <= positions[0][1]:
                    self.position = 'exon'
                else:
                    self.position = 'intron'
        else:
            self.position = '-'

    def build_genes_dict(self):
        """
            Fetch QuerySet of genes from data base.
            Sort it first by chromosome name,
            next by start value.
            Build dictionary containing chromosome names as keys
            and lists of genes on given chromosome as values.
        """
        database = check_db_status()
        db = EnsemblVersion.objects.get(db_name=self.query.db)
        if database == 'main':
            organism = OrganismMain.objects.get(scientific_name=self.query.organism, db=db).pk
            temp = GeneMain.objects.filter(
                        organism_id=organism).order_by(
                        'chr_name', 'start')
        elif database == 'backup':
            organism = OrganismBackup.objects.get(scientific_name=self.query.organism, db=db).pk
            temp = GeneBackup.objects.filter(
                        organism_id=organism).order_by(
                        'chr_name', 'start')
        for gene in temp:
            if gene.chr_name not in self.genes_dict:
                self.genes_dict[gene.chr_name] = []
            self.genes_dict[gene.chr_name].append(gene)


def check_db_status():
    """
        Check database status to get data from avaible database.
    """
    status = DataBaseStatus.objects.get(pk=1)
    if status.get_status() == 'stable':
        database = 'main'
    elif status.get_status() == 'update':
        database = 'backup'
    return database


def unify_strand(strand):
    """
        Change strand symbol in query.data to match database annotation.
    """
    if strand == '-' or strand == '-1':
        return '-'
    elif strand == '+' or strand == '1':
        return '+'

if __name__ == '__main__':
    QUERY = Annotations()
    QUERY.generate_annotation(sys.argv[1])
