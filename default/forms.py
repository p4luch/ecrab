from django import forms
import time
from .models import FileStatus, Query
import os
from ecrab.settings import BASE_DIR
import re


def load_sep():
    separators = []
    separators.append(('whitespace', 'tabulation/space'))
    separators.append(('tab', 'tabulation (\\t)'))
    separators.append(('space', 'space ( )'))
    separators.append(('comma', 'comma (,)'))
    separators.append(('scol', 'semicolon (;)'))
    return separators


def load_db():
    sources = []
    sources.append(('main', 'Ensembl'))
    sources.append(('metazoa', 'Ensembl Metazoa'))
    sources.append(('plants', 'Ensembl Plants'))
    sources.append(('fungi', 'Ensembl Fungi'))
    sources.append(('protists', 'Ensembl Protists'))
    return sources


SEPARATOR = load_sep()
SOURCES = load_db()


class QueryForm(forms.Form):

    database = forms.ChoiceField(
                                 choices=SOURCES,
                                 widget=forms.Select(
                                            attrs={'class': 'form-control'}))

    strand = forms.BooleanField(
                                required=False,
                                widget=forms.CheckboxInput(
                                                           attrs={
                                                            'class':
                                                            'example'}))

    ontology = forms.BooleanField(
                                  required=False,
                                  widget=forms.CheckboxInput(
                                                             attrs={
                                                                'class':
                                                                'example'}))
    separator = forms.ChoiceField(
                                  required=False,
                                  choices=SEPARATOR,
                                  widget=forms.Select(
                                             attrs={'class': 'from-control'}))


    exons = forms.BooleanField(
                               required=False,
                               widget=forms.CheckboxInput(
                                                          attrs={
                                                            'class':
                                                            'example'}))

    data = forms.CharField(
                           required=False,
                           widget=forms.Textarea(
                            attrs={'class': 'form-control',
                                   'placeholder': 'Paste data here...'}))

    email = forms.EmailField(
                             required=False,
                             widget=forms.EmailInput(
                                attrs={'class': 'form-control',
                                       'placeholder': 'Enter e-mail'}))

    file = forms.FileField(required=False)

    timestamp = forms.CharField(
                                required=False,
                                initial=time.time(),
                                widget=forms.HiddenInput())

    bed = forms.BooleanField(
                             required=False,
                             initial=True,
                             widget=forms.CheckboxInput(
                                                        attrs={
                                                            'class': 'example',
                                                            'id': 'bed'}))

    custom = forms.BooleanField(
                                required=False,
                                widget=forms.CheckboxInput(
                                                           attrs={'id':
                                                                  'custom'}))

    c_strand = forms.IntegerField(
                                  required=False,
                                  min_value=1,
                                  widget=forms.NumberInput(
                                                           attrs={
                                                            'class':
                                                            'col-md-4'}))

    c_start = forms.IntegerField(
                                 required=False,
                                 min_value=1,
                                 widget=forms.NumberInput(
                                                          attrs={'class':
                                                                 'col-md-4'}))

    c_end = forms.IntegerField(
                               required=False,
                               min_value=1,
                               widget=forms.NumberInput(
                                                        attrs={
                                                            'class':
                                                            'col-md-4'}))

    c_chromosome = forms.IntegerField(
                                      required=False,
                                      min_value=1,
                                      widget=forms.NumberInput(
                                                               attrs={
                                                                'class':
                                                                'col-md-4'}))

    def is_valid(self):
        line_counter = 0
        custom_no_fields = False
        valid = super(QueryForm, self).is_valid()

        if not valid:
            return valid

        f_stat = FileStatus.objects.get(pk=1)
        if f_stat == 'update':
            self._errors['file_update'] = "Server is updating files. Please try again in 10 minutes."
            return False

        to_return = True
        bed3 = True
        c_columns = []
        sep = {
            'tab': '\t',
            'space': ' ',
            'comma': ',',
            'scol': ';',
            'whitespace': 'white'
        }
        # Custom validation
        if not self.cleaned_data['file'] and self.cleaned_data['data'] == '':
            self._errors['no_data'] = "Please provide data to annotation."
            to_return = False

        if self.cleaned_data['file'] and self.cleaned_data['data'] != '':
            self._errors['to_much_data'] = "Please paste data to annotate \
                    OR upload file."
            to_return = False

        if not self.cleaned_data['bed'] and not self.cleaned_data['custom']:
            self._errors['no_format'] = "Please specify file format. \
                    BED \or Custom."
            return False

        if self.cleaned_data['custom']:
            c_columns = self.get_c_columns()
            if not self.cleaned_data['c_chromosome']:
                self._errors['no_c_chromosome'] = "Please specify \
                        chromosome column."
                to_return = False

            if not self.cleaned_data['c_strand'] and self.cleaned_data['strand']:
                self._errors['no_c_strand'] = "Please specify strand column."
                to_return = False

            if not self.cleaned_data['c_start']:
                self._errors['no_c_start'] = "Please specify start column."
                to_return = False

            if not self.cleaned_data['c_end']:
                self._errors['no_c_end'] = "Please specify end column."
                to_return = False

            no_duplicates = list(set(c_columns))
            if len(no_duplicates) < len(c_columns):
                self._errors['same_columns'] = "Custom file columns numbers \
                        need to be different."
                to_return = False

        if not self.cleaned_data['file']:
            c_columns = self.get_c_columns()
            print("if not self.cleaned_data['file']: {}".format(c_columns))
            for line in self.cleaned_data['data'].split('\n'):
                line_counter += 1
                if line.strip('\r\n') == '':
                    continue
                if sep[self.cleaned_data['separator']] == 'white':
                    columns = re.split("\s+", line.strip('\r\n'))
                elif sep[self.cleaned_data['separator']] == ' ':
                    columns = re.split(" +", line.strip('\r\n'))
                else:
                    columns = line.strip('\r\n').split(sep[self.cleaned_data['separator']])
                if self.cleaned_data['custom']:
                    if max(c_columns) > len(columns):
                        self._errors['no_cols'] = "File doesn't contain \
                                specified number of columns. Please make sure that\
                                 custom numbers correspond to columns in file."
                        to_return = False
                for item in columns:
                    item = item.strip('\r\n')
                for item in columns:
                    if item.strip() == '':
                        self._errors['missing_data'] = "Data contains empty fields,\
                                please check and submit again."
                        to_return = False
                if len(columns) < 3:
                    self._errors['no_bed3'] = "Please check if all lines\
                             have at least 3 columns or the column separator was set correctly."
                    bed3 = False
                    to_return = False
                if self.cleaned_data['strand']:
                    # Check if strand specific is True and data have 6 columns
                    if len(columns) < 6:
                        self._errors['no_bed6'] = "Not enough data \
                                to perform Strand specific search. Check if all \
                                lines have at least 6 columns."
                        to_return = False
                    elif len(columns) >= 6:
                        # Check if 6th column is from [+, -, -1, 1]
                        if columns[5].strip() not in ['+', '-', '-1', '1']:
                            self._errors['no_strand'] = "Column no. 6 doesn't \
                                    contain valid strand symbol(+, -, -1, 1)."
                            to_return = False
                if bed3:
                    if '' not in (columns[1], columns[2]) and \
                                    int(columns[1]) > int(columns[2]):
                        # Check if all data have start less than end.
                        self._errors['coord'] = "The start position(column 2) is highier than \
                                end position(column 3. Please remember that start \
                                             have to be always lower than end."
                        to_return = False
        else:
            fname = "{0}{1}".format(self.cleaned_data['file'].name,
                                    self.cleaned_data['timestamp'])
            with open(os.path.join('/', BASE_DIR, 'media', fname), 'w') as destination:
                for chunk in self.cleaned_data['file'].chunks():
                    destination.write(chunk.decode("utf-8") )
            with open(os.path.join('/', BASE_DIR, 'media', fname), 'r') as check:
                for line in check:
                    line_counter += 1
                    if line.strip('\r\n') == '':
                        continue
                    line = line.strip()

                    if sep[self.cleaned_data['separator']] == 'white':
                        columns = re.split('\s+', line.strip('\r\n'))
                    elif sep[self.cleaned_data['separator']] == ' ':
                        columns = re.split(" +", line.strip('\r\n'))
                    else:
                        columns = line.strip('\r\n').split(sep[self.cleaned_data['separator']])
                    if self.cleaned_data['custom']:
                        if max(c_columns) > len(columns):
                            self._errors['no_cols'] = "Text field doesn't \
                                    contain specified number of columns. Please make \
                                    sure that custom numbers correspond to columns \
                                    in text field."
                            to_return = False
                    for item in columns:
                        if item.strip() == '':
                            self._errors['missing_data'] = "Data contains empty fields,\
                                please check and submit again."
                            to_return = False
                    if len(columns) < 3:
                        self._errors['no_bed3'] = "Please check if all lines \
                                have at least 3 columns or column separator was set correctly."
                        bed3 = False
                        to_return = False
                    if self.cleaned_data['strand']:
                        # Check if strand specific is True and data have 6 cols
                        if len(columns) < 6:
                            self._errors['no_bed6'] = "Not enough data \
                                to perform Strand specific search. Check if all \
                                lines have at least 6 columns."
                            to_return = False
                        elif len(columns) >= 6:
                            # Check if 6th column is from [+, -, -1, 1]
                            if columns[5].strip() not in ['+', '-', '-1', '1']:
                                self._errors['no_strand'] = "Column no. 6 doesn't \
                                    contain valid strand symbol(+, -, -1, 1)."
                                to_return = False
                    if bed3:
                        if '' not in (columns[1], columns[2]) and \
                                        int(columns[1]) > int(columns[2]):
                            # Check if all data have start less than end.
                            self._errors['coord'] = "The start position(column 2) is highier than \
                                end position(column 3. Please remember that start have\
                                 to be always lower than end."
                            to_return = False
        if line_counter > 100000:
            self._errors['length'] = "Data too big. Please annotate \
                    max. 50 000 lines of data."
            to_return = False
        return to_return

    def get_c_columns(self):
        to_ret = []
        if not self.cleaned_data['c_chromosome']:
            to_ret.append(-1)
        else:
            to_ret.append(self.cleaned_data['c_chromosome'])
        if not self.cleaned_data['c_start']:
            to_ret.append(-1)
        else:
            to_ret.append(self.cleaned_data['c_start'])
        if not self.cleaned_data['c_end']:
            to_ret.append(-1)
        else:
            to_ret.append(self.cleaned_data['c_end'])
        if not self.cleaned_data['c_strand']:
            to_ret.append(-1)
        else:
            to_ret.append(self.cleaned_data['c_strand'])
        return to_ret


class RetrieveForm(forms.Form):
    job_id = forms.IntegerField()

    def is_valid(self):
        to_return = True
        valid = super(RetrieveForm, self).is_valid()

        if not valid:
            return valid

        if self.cleaned_data['job_id']:
            try:
                q = Query.objects.get(slug=self.cleaned_data['job_id'])
            except Query.DoesNotExist:
                self._errors['not_exists'] = "Query with id {0} does not exist.".format(self.cleaned_data['job_id'])
                to_return = False
        return to_return