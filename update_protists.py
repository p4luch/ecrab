import sys
import django
import os

from update_script import Ensembl

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ecrab.settings")
django.setup()

from default.models import EnsemblVersion


class ProtistsEnsembl():
    def __init__(self):
        self.ensembl = 'protists'
        self.host = 'ftp://ftp.ensemblgenomes.org'
        self.dir = 'pub/current/protists/mysql'
        self.current_db_version = EnsemblVersion.objects.get(db_name='protists').db_version
        self.db_obj = EnsemblVersion.objects.get(db_name='protists')
        self.PROTISTS = ''

    def manager(self):
        self.PROTISTS = Ensembl()
        self.update_params()
        self.PROTISTS.manager()

    def update_params(self):
        self.PROTISTS.dir = self.dir
        self.PROTISTS.host = self.host
        self.PROTISTS.current_db_version = self.current_db_version
        self.PROTISTS.db_obj = self.db_obj
        self.PROTISTS.ensembl = self.ensembl
