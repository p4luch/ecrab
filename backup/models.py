from django.db import models
from default.models import EnsemblVersion

# Create your models here.


class BiotypeBackup(models.Model):
    biotype_name = models.CharField(max_length=50)
    db = models.ForeignKey(EnsemblVersion)


class OrganismBackup(models.Model):
    scientific_name = models.CharField(max_length=50)
    db = models.ForeignKey(EnsemblVersion)


class GeneBackup(models.Model):
    organism = models.ForeignKey(OrganismBackup)
    gene_id = models.CharField(max_length=50)
    start = models.IntegerField()
    end = models.IntegerField()
    chr_name = models.CharField(max_length=100)
    asso_gene_name = models.CharField(max_length=50)
    strand = models.CharField(max_length=1)
    g_type = models.ForeignKey(BiotypeBackup)
    description = models.TextField()
    db = models.ForeignKey(EnsemblVersion)

    def __str__(self):
        return self.organism.scientific_name+' '+self.chr_name+' '+self.gene_id

    def get_description(self):
        return 'SEP\tSEP'.join([
                         self.chr_name,
                         self.gene_id,
                         self.asso_gene_name,
                         str(self.start),
                         str(self.end),
                         self.strand,
                         self.g_type.biotype_name,
                         self.description
                         ])
