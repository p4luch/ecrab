class SelectBackupDatabase(object):
    """
    A router to control all database operations on models in the
    backup application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read backup models go to backup_db.
        """
        if model._meta.app_label == 'backup':
            return 'backup_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write backup models go to backup_db.
        """
        if model._meta.app_label == 'backup':
            return 'backup_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the backup app is involved.
        """
        if obj1._meta.app_label == 'backup' or \
                obj2._meta.app_label == 'backup':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the main app only appears in the 'backup_db'
        database.
        """
        if app_label == 'backup':
            return db == 'backup_db'
        return None
